import express from 'express'
import cors from 'cors'
import morgan from 'morgan'
import { sequelize } from './models'
import config from './config/config'
import routes from './routes/index'
import enforce from 'express-sslify'

const app = express()
app.use(morgan('combined'))
app.use(enforce.HTTPS({ trustProtoHeader: true }))
app.use(express.json())
app.use(cors())

routes(app)


sequelize.sync()
    .then(() => {
        const server = app.listen(config.port)
        console.log(`Server started on port ${config.port}`)

        //Gracefull Shutdown
        function gracefullShutdown() {
            server.close(() => {
                sequelize.close()
                .then(() => {
                    console.log('Database connexion closed')
                }) 
                console.log('Server closed')
            })
        }
        process.on('SIGINT', gracefullShutdown)
        process.on('SIGTERM', gracefullShutdown)
        process.on('SIGHUP', gracefullShutdown)
    })
