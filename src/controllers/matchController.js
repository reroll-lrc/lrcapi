
import Match from '../models/Match'
import Team from '../models/Team'


export default {
    async getAllMatches (req, res) {
        try{
            
            const matchs = await Match.findAll({
                order: ['id'],
            })

            for (let match of matchs) {
                match.team1 = await Team.findByPk(match.team1)
                match.team2 = await Team.findByPk(match.team2)
            }
            
            res.status(200).send(matchs)
        } catch (error) {
            res.status(500).send({
                error: 'Error trying to get matches'
            })
        }
    },

    async getMatchById (req, res) {
        try {
            let match = await Match.findByPk(req.params.matchId)
            match.team1 = await Team.findByPk(match.team1)
            match.team2 = await Team.findByPk(match.team2)
            res.status(200).send(match)
        } catch (error) {
            res.status(500).send({
                error: 'Error trying to get match'
            })
        }
    },

    async editMatch (req, res) {
        try {
            let match = await Match.findByPk(req.params.matchId)
            await match.update(req.body)
            res.status(200).send(req.body)
        } catch (error) {
            res.status(500).send({
                error: 'Error trying to modify Match'
            })
        }
    }
}