import Team from '../models/Team'
import Manager from '../models/Manager'
import Player from '../models/Player'

export default {
    async getAllTeams(req, res) {
        try {

            const teams = await Team.findAll({
                order: ['id'],
                include: [{
                    model: Manager,
                    attributes: ['discord']
                }]
            })
            for (let i in teams) {
                teams[i].dataValues.players = await teams[i].getPlayers()
            }
            res.status(200).send(teams)
        } catch (error) {
            res.status(500).send({
                error: 'Error trying to get teams'
            })
        }
    },


    async getManagerTeam(req, res) {
        try {
            const manager = await Manager.findByPk(req.params.managerId)
            const team = await manager.getTeam()
            team.dataValues.Manager = manager
            team.dataValues.Players = await team.getPlayers()
            res.status(201).send(team)
        } catch (error) {
            res.status(500).send({
                error: 'Error trying to get team'
            })
        }
    },

    async editTeam(req, res) {
        try {
            await Team.update(req.body, {
                where: {
                    id: req.params.teamId
                }
            })
            res.status(200).send(req.body)
        } catch (error) {
            res.status(500).send({
                error: 'Error trying to modify team'
            })
        }
    },

    async deletePlayer(req, res) {
        try {
            await Player.destroy({
                where: {
                    id: req.params.playerId
                }
            })
            res.status(200).send()
        } catch (error) {
            res.status(500).send({
                error: 'Error trying to delete player.'
            })
        }
    },

    async createPlayer(req, res) {
        try {
            const p = await Player.create({
                "ingameName": req.body.player,
                "TeamId": req.body.team
            })
            res.status(201).send(p)
        } catch (error) {
            res.status(500).send({
                error: 'Error trying to delete player.'
            })
        }
    },

    //to disable once team history will be created
    async deleteTeam(req, res) {
        try {
            await Team.destroy({
                where: {
                    id: req.params.teamId
                }
            })
            res.status(200).send({
                message: 'Team deleted'
            })
        } catch (error) {
            res.status(500).send({
                error: 'Error trying to delete team'
            })
        }
    }
}