import Manager from '../models/Manager'
import { sequelize } from '../models/index'
import jwt from 'jsonwebtoken'
import config from '../config/config'
import Team from '../models/Team'
import Player from '../models/Player'

function jwtSignManager(manager) {
    return jwt.sign(
        {
            id: manager.id,
            isAdmin: manager.isAdmin
        },
        config.authentication.jwt,
        { expiresIn: 3600 * 24 } //24h
    )
}

export default {
    async register(req, res) {
        const t = await sequelize.transaction();
        try {
            const manager = await Manager.create(req.body.Manager, { transaction: t })
            let newTeam = req.body.Team
            newTeam.ManagerId = manager.dataValues.id
            newTeam.accepted = false
            const team = await Team.create(newTeam, { transaction: t })

            for (let i in req.body.Players) {
                let newPlayer = { "ingameName": req.body.Players[i] }
                newPlayer.TeamId = team.id
                await Player.create(newPlayer, { transaction: t })
            }
            await t.commit();
            res.status(201).send("Team has been created")


        } catch (error) {
            await t.rollback()
            res.status(409).send({
                error: error.parent.detail 
            })
        }

    },

    async login(req, res) {
        try {
            const { email, password } = req.body
            const manager = await Manager.findOne({ where: { email: email } })

            if (!manager) {
                return res.status(401).send({ error: 'Wrong credentials' })
            }

            const isPasswordValid = await manager.comparePassword(password)

            if (!isPasswordValid) {
                return res.status(401).send({ error: 'Wrong credentials' })
            }

            res.status(200).send({
                manager: manager.toJSON(),
                token: jwtSignManager(manager.toJSON())
            })

        } catch (error) {
            res.status(500).send({ error: 'An error occured' })
        }
    },

    async changeAccount(req, res) {
        try {
            //get the manager 
            let manager = await Manager.findOne({
                where: {
                    id:req.params.managerId
                }
            })
            //update the manager instance for emit beforeUpdate Hook
            await manager.update(req.body)
            res.status(200).send(req.body)
        } catch (error) {
            res.status(500).send({
                error: 'Error trying to modify Manager'
            })
        }
    }
}