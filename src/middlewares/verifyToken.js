import jwt from 'jsonwebtoken'
import config from '../config/config'

export default function verifyToken(req, res, next) {
    try {
        const token = req.headers.authorization.split(' ')[1]
        const decodedToken = jwt.verify(token, config.authentication.jwt)
        const id = decodedToken.id
        if (req.body.id && req.body.id !== id) {
          throw 'Invalid ID'
        } else {
          next()
        }
      } catch {
        res.status(401).send({
          error: 'Unauthorized'
        })
      }
    }