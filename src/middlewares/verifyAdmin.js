import jwt from "jsonwebtoken"
import config from "../config/config"
import Manager from "../models/Manager";

export default async function verifyAdmin(req, res, next) {
    try {
        const token = req.headers.authorization.split(' ')[1]
        const decodedToken = jwt.verify(token, config.authentication.jwt)

        const manager = Manager.findByPk(decodedToken.id);
        if(manager && manager.isAdmin) {
            next()
        }
        else {
            res.status(403).send()
        }
      } catch {
        res.status(401).send({
          error: 'Unauthorized'
        })
      }
    }