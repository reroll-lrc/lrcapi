import {Model, DataTypes} from 'sequelize'
import bcrypt from 'bcrypt'


async function hashPassword (manager) {
    const SALT_ROUNDS = 10
    if(!manager.changed('password')){
        return
    }
    try{
        let salt = await bcrypt.genSalt(SALT_ROUNDS)
        let hashedPassword = await bcrypt.hash(manager.password,salt)
        manager.setDataValue('password',hashedPassword)
    } catch (err){
        console.log(err)
    }
}

class Manager extends Model {
    static init(sequelize) {
        return super.init(
            {
                email: {
                    type: DataTypes.STRING,
                    unique: true,
                    allowNull: false
                 },
                 password: DataTypes.STRING,
                 discord: DataTypes.STRING,
                 isAdmin:{
                     type: DataTypes.BOOLEAN,
                     allowNull:false
                 }
                },{
                    hooks: {
                        beforeUpdate: hashPassword,
                        beforeCreate: hashPassword
                    },
                    sequelize, 
                    timestamps: false}
        )
    }

    async comparePassword (password) {
       try{
        let compare = await bcrypt.compare(password,this.password)
        return compare
       } catch (err){
           console.log(err)
        }
    }
}

Manager.associate = function(models) {
    Manager.hasOne(models.Team)
}

export default Manager