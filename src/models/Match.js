import { Model, DataTypes } from 'sequelize'

class Match extends Model {
    static init(sequelize) {
        return super.init(
            {
                name: DataTypes.STRING,
                team1: {
                    type: DataTypes.INTEGER,
                    allowNull: true
                },
                team2: {
                    type: DataTypes.INTEGER,
                    allowNull: true
                },
                score1: {
                    type: DataTypes.INTEGER,
                    allowNull: true
                 },
                 score2: {
                    type: DataTypes.INTEGER,
                    allowNull: true
                 },
                 state : {
                     type : DataTypes.STRING,
                     allowNull: false
                 }
            }, { sequelize }
        )
    }
}

export default Match