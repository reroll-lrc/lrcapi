import { Sequelize } from 'sequelize'
import config from '../config/config'

import Manager from './Manager'
import Team from './Team'
import Player from './Player'
import Match from './Match'


//Create database
const sequelize = new Sequelize(
    config.db.database,
    config.db.user,
    config.db.password,
    config.db.options
)

//Test connection
async function test(){
    try {
        await sequelize.authenticate()
        console.log('Connection has been established successfully.')
      } catch (error) {
        console.error('Unable to connect to the database:', error)
      }
}
test()

//Create Models and Associations
const models = {
  Manager: Manager.init(sequelize),
  Team: Team.init(sequelize),
  Player: Player.init(sequelize),
  Match: Match.init(sequelize),
}

Object.values(models)
  .filter(model => typeof model.associate === "function")
  .forEach(model => model.associate(models))

export {models, sequelize}