import { Model, DataTypes } from 'sequelize'

class Player extends Model {
    static init(sequelize) {
        return super.init(
            {
                ingameName: {
                    type: DataTypes.STRING(50),
                    unique: true,
                    allowNull: false
                }
            }, { sequelize, timestamps: false }
        )
    }
}

Player.associate = function (models) {
    Player.belongsTo(models.Team)  
}

export default Player