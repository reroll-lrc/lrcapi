import {Model, DataTypes} from 'sequelize'

class Team extends Model {
    static init(sequelize) {
        return super.init(
            {
               teamName: {
                  type: DataTypes.STRING(50),
                  unique: true,
                  allowNull: false
               },
               tag: {
                   type: DataTypes.STRING(5),
                },
                logo: {
                   type: DataTypes.STRING,
                   allowNull: true,
       
                },
                accepted: DataTypes.BOOLEAN,
                opgg: {
                   type: DataTypes.TEXT,
                    allowNull: true
                }
            },{sequelize}
        )
    }
}

Team.associate = function(models) {
    Team.belongsTo(models.Manager)
    Team.hasMany(models.Player)
}
export default Team