const config = {
    port: process.env.PORT || 8081,
    db: {
        database: process.env.POSTGRESQL_ADDON_DB || 'lrc',
        user: process.env.POSTGRESQL_ADDON_USER || 'postgres',
        password: process.env.POSTGRESQL_ADDON_PASSWORD || 'postgres',
        options: {
            dialect: process.env.DIALECT || 'postgres',
            host: process.env.POSTGRESQL_ADDON_HOST || 'localhost',
            port: process.env.POSTGRESQL_ADDON_PORT || '5433'
        },
    },
    authentication: {
        jwt: process.env.JWT_SECRET || 'secret'
    }
}

export default config