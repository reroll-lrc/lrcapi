import controller from '../controllers/controller'
import matchController from '../controllers/matchController'
import teamController from '../controllers/teamController'

import verifyToken from '../middlewares/verifyToken'
import verifyAdmin from '../middlewares/verifyAdmin'

export default (app) => {

    //authenticationRoutes
    app.post('/register', controller.register)
    app.post('/login', controller.login)
    app.put('/managers/:managerId', controller.changeAccount)

    //teamRoutes
    app.get('/teams', teamController.getAllTeams)
    app.get('/manager/:managerId/team', verifyToken, teamController.getManagerTeam) //auth
    app.put('/teams/:teamId', verifyToken, teamController.editTeam) //auth
    app.delete('/teams/:teamId', verifyAdmin, teamController.deleteTeam) //admin


    //PlayerRoutes
    app.post('/players', verifyToken, teamController.createPlayer) //auth
    app.delete('/players/:playerId', verifyToken, teamController.deletePlayer) //auth

    app.get('/matches', matchController.getAllMatches)
    app.get(`/matches/:matchId`, matchController.getMatchById)
    app.put('/matches/:matchId', matchController.editMatch)

    app.get('/check', verifyToken, function (req, res) {
        res.send('ok')
    })
}