Express api for the League of Legends Reroll Cup website.

Clone repository then move in the folder.

Install dependencies

`npm install`

Start database (you need [docker-compose](https://docs.docker.com/compose/install/) installed on your computer)

`docker-compose up -d`

Start api

`npm run serverstart`
